<?php
	//include all DAO files
	require_once('class/sql/Connection.class.php');
	require_once('class/sql/ConnectionFactory.class.php');
	require_once('class/sql/ConnectionProperty.class.php');
	require_once('class/sql/QueryExecutor.class.php');
	require_once('class/sql/Transaction.class.php');
	require_once('class/sql/SqlQuery.class.php');
	require_once('class/core/ArrayList.class.php');
	require_once('class/dao/DAOFactory.class.php');
 	
	require_once('class/dao/GameDAO.class.php');
	require_once('class/dto/Game.class.php');
	require_once('class/mysql/GameMySqlDAO.class.php');
	require_once('class/mysql/ext/GameMySqlExtDAO.class.php');
	require_once('class/dao/InventoryitemDAO.class.php');
	require_once('class/dto/Inventoryitem.class.php');
	require_once('class/mysql/InventoryitemMySqlDAO.class.php');
	require_once('class/mysql/ext/InventoryitemMySqlExtDAO.class.php');
	require_once('class/dao/ItemDAO.class.php');
	require_once('class/dto/Item.class.php');
	require_once('class/mysql/ItemMySqlDAO.class.php');
	require_once('class/mysql/ext/ItemMySqlExtDAO.class.php');
	require_once('class/dao/ItemlocationDAO.class.php');
	require_once('class/dto/Itemlocation.class.php');
	require_once('class/mysql/ItemlocationMySqlDAO.class.php');
	require_once('class/mysql/ext/ItemlocationMySqlExtDAO.class.php');
	require_once('class/dao/TileDAO.class.php');
	require_once('class/dto/Tile.class.php');
	require_once('class/mysql/TileMySqlDAO.class.php');
	require_once('class/mysql/ext/TileMySqlExtDAO.class.php');
	require_once('class/dao/UserDAO.class.php');
	require_once('class/dto/User.class.php');
	require_once('class/mysql/UserMySqlDAO.class.php');
	require_once('class/mysql/ext/UserMySqlExtDAO.class.php');

?>