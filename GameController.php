<?php
require_once ('include_dao.php');
require_once ('TileDaoExt.php');
require_once ('ItemLocationDaoExt.php');
require_once ('InventoryItemDaoExt.php');

/** Changelog
 *  ## Fri, Jun 6 2014
 *  - Started implementation work
 *  - Changed name of encodeDataForClient() => encodeClientData()
 *  - Changed param $actionName:string => $actionIndex:int for
 * performActionOnItem()
 * */

class GameController {
	private static $s_Instance = null;
	private $m_DebugModeEnabled;
	public $m_log = null;
	public $m_isLoggedIn = null;
	private $m_GameInfoDao = null;
	private $m_Game = null;
	private $m_TileDao = null;
	private $m_Tile = null;
	private $m_User = null;
	private $m_UserDao = null;
	private $m_ItemDao = null;
	private $m_ItemLocationDao = null;
	private $m_InventoryItemDao = null;
	private $m_Item = null;

	private function __construct() {
		$this->m_log = array();
		$this->m_DebugModeEnabled = true;
	}

	function __destruct() {

	}

	static function getInstance() {
		if(!isset(Gamecontroller::$s_Instance) || !is_null(GameController::$s_Instance)) {
			Gamecontroller::$s_Instance = new GameController();
		}
		return Gamecontroller::$s_Instance;
	}

	//@returns void
	//@params string $command
	function processCommand($raw_command) {
		//session_destroy();
		$this->log("&gt $raw_command");
		$params = explode(' ', $raw_command);
		$command = $params[0];
		unset($params[0]);
		$params = array_values($params);

		if($this->m_isLoggedIn) {//logged in switch
			//need to retreive all this info here because of help command
			if(!isset($this->m_User)) {
				if(!isset($this->m_UserDao)) {
					$this->m_UserDao = DAOFactory::getUserDAO();
				}
				$this->m_User = $this->m_UserDao->load($_SESSION['userid']);
				if(!isset($this->m_User)) {
					$this->m_isLoggedIn = false;
					unset($_SESSION['userid']);
					session_destroy();
					header("Location: index.php");
				}
			}

			$game = $this->getGameFromUserId($this->m_User->userUid);
			if(isset($game)) {
				//readability
				$tileCurrentUid = $game->tileUid;
				// $this->log('current tile uid: ' . $tileCurrentUid, true);
				if(!isset($this->m_TileDao)) {
					$this->m_TileDao = new TileDaoExt();
				}
				$this->m_Tile = $this->m_TileDao->queryByTileUid($tileCurrentUid);
				if(isset($this->m_Tile[0])) {//make is easier to read
					$this->m_Tile = $this->m_Tile[0];
				}
				switch($command) {

					case 'help':
						$availableCommands = "available commands: help, look, use, examine, inventory, take, drop, logout";
						//add any available navigation commands
						if(isset($this->m_Tile->northtileUid)) {
							$availableCommands .= ', north';
						}
						if(isset($this->m_Tile->southtileUid)) {
							$availableCommands .= ', south';
						}
						if(isset($this->m_Tile->easttileUid)) {
							$availableCommands .= ', east';
						}
						if(isset($this->m_Tile->westtileUid)) {
							$availableCommands .= ', west';
						}

						$this->log($availableCommands);

						break;
					case 'logout': {
						session_destroy();
						$this->m_User = null;
						$this->m_isLoggedIn = null;
						$this->m_GameInfoDao = null;
						$this->m_TileDao = null;
						$this->m_Tile = null;
						$this->m_User = null;
						$this->m_UserDao = null;
						header("Location: index.php");
						break;
					}
					/** item related commands */
					case 'pickup':
					case 'take':
						$this->pickupItem(implode(' ', $params));
						break;
					case 'drop':
						$this->dropItem(implode(' ', $params));
						break;
					case 'inv':
					case 'inventory':
						$this->checkInventory();
						break;
					case 'examine':
						$this->examineItem(implode(' ', $params));
						break;
					case 'use':
						$inventoryItems = $this->getInventoryitemsForGame($game);
						$tileItems = $this->getItemsForGameOnTile($game);
						$usableItems = array_merge($inventoryItems, $tileItems);
						if(!isset($usableItems[0])) {
							$this->log("There are no items you can use right now");
						} else {
							if(!isset($params[0])) {
								$itemNames = array();
								foreach($usableItems as $item) {//add all visible items to the list
									if($item->isVisible) {
										array_push($itemNames, $item->name);
									}
								}
								$itemsList = implode(', ', $itemNames);
								$this->log("You can use $itemsList");
							} else {
								$itemName = implode(' ', $params);
								foreach($usableItems as $item) {
									if($item->name == $itemName && $item->isVisible == 1) {
										switch($item->itemUid) {
											case 5:
												//my keys

												switch($this->m_Game->tileUid) {
													case 1:
													case 2:
														$alreadyUnlocked = false;
														foreach($inventoryItems as $invItem) {
															if($invItem->itemUid == 7 || $invItem->itemUid == 8) {
																$alreadyUnlocked = true;
															}
														}
														if(!$alreadyUnlocked) {
															$inventoryItem = new InventoryItem;
															$inventoryItem->gameUid = $game->gameUid;
															$inventoryItem->itemItemUid = 7;
															@$this->m_InventoryItemDao->insert($inventoryItem);
															unset($inventoryItem);
															$inventoryItem = new InventoryItem;
															$inventoryItem->gameUid = $game->gameUid;
															$inventoryItem->itemItemUid = 8;
															@$this->m_InventoryItemDao->insert($inventoryItem);
															unset($inventoryItem);

															$this->log("$item->name unlocked your front door.");
														} else {
															$this->log("your front door is already unlocked.");

														}
														return;

														break;

													default:
														$this->examineItem(implode(' ', $params));
														break;
												}
												break;
											case 13:
												//neighbor keys
												switch($this->m_Game->tileUid) {
													case 3:
													case 4:
														$alreadyUnlocked = false;
														foreach($inventoryItems as $invItem) {
															if($invItem->itemUid == 11 || $invItem->itemUid == 21) {
																$alreadyUnlocked = true;
															}
														}
														if(!$alreadyUnlocked) {
															$inventoryItem = new InventoryItem;
															$inventoryItem->gameUid = $game->gameUid;
															$inventoryItem->itemItemUid = 11;
															@$this->m_InventoryItemDao->insert($inventoryItem);
															unset($inventoryItem);
															$inventoryItem = new InventoryItem;
															$inventoryItem->gameUid = $game->gameUid;
															$inventoryItem->itemItemUid = 21;
															@$this->m_InventoryItemDao->insert($inventoryItem);
															unset($inventoryItem);

															$this->log("$item->name unlocked your neighbor's front door.");
														} else {
															$this->log("your neighbor's front door is already unlocked.");

														}
														return;

														break;
													default:
														$this->examineItem(implode(' ', $params));
														break;
												}
												break;
											case 19:
												if($this->m_Game->tileUid == 5) {
													//the radio
													$this->log("You turn off the radio. DAMN! Finally!");
													$this->log("&nbsp;");
													$this->log("======================================");
													$this->log("==  GAME OVER, Thanks for playing!  ==");
													$this->log("======================================");
													return;
												}
												$this->examineItem(implode(' ', $params));
												break;
											default:
												$this->log("This $item->name doesn't do anything useful here");
												return;
										}
									}
								}
								$this->log("What's this '$itemName' you speak of?");
							}
						}

						break;
					/** navigation commands */
					case 'north':
					case 'east':
					case 'south':
					case 'west': {
						//find locked doors on this tile
						$lockedDoors = $this->getLockedDoors($game);

						$issuedInvalidNavCommand = true;
						$blocknavigation = false;

						switch($command) {
							case 'north':
								if(isset($this->m_Tile->northtileUid)) {
									$this->log('north OK', true);
									switch($this->m_Tile->tileUid) {
										case 1:
											//front door check - inside
											foreach($lockedDoors as $door) {
												if($door->itemUid == 7) {//block navigation
													$blocknavigation = true;
												}
											}
											break;
										case 4:
											//neighbor door check - inside
											foreach($lockedDoors as $door) {
												if($door->itemUid == 21) {//block navigation
													$blocknavigation = true;
												}
											}
											break;
										default:
											break;
									}

									if(!$blocknavigation) {
										$game->tileUid = $this->m_Tile->northtileUid;
									}
									//
									$issuedInvalidNavCommand = false;

								}
								break;
							case 'east':
								if(isset($this->m_Tile->easttileUid)) {
									$this->log('east OK', true);
									$game->tileUid = $this->m_Tile->easttileUid;
									$issuedInvalidNavCommand = false;
									//no doors facing east, lol
								}
								break;
							case 'south':
								if(isset($this->m_Tile->southtileUid)) {
									$this->log('south OK', true);

									switch($this->m_Tile->tileUid) {
										case 2:
											//front door check - outside
											foreach($lockedDoors as $door) {
												if($door->itemUid == 8) {//block navigation
													$blocknavigation = true;
												}
											}
											break;
										case 3:
											//neighbor door check - outside
											foreach($lockedDoors as $door) {
												if($door->itemUid == 11) {//block navigation
													$blocknavigation = true;
												}
											}
											break;
										default:
											break;
									}

									if(!$blocknavigation) {
										$game->tileUid = $this->m_Tile->southtileUid;
									}
									$issuedInvalidNavCommand = false;

								}
								break;
							case 'west':
								if(isset($this->m_Tile->westtileUid)) {
									$this->log('west OK', true);
									$game->tileUid = $this->m_Tile->westtileUid;
									$issuedInvalidNavCommand = false;

								}
								break;
							default:
								die('inconsistent navigation command');
								break;
						}
						if(!$issuedInvalidNavCommand) {
							if(!$blocknavigation) {
								//reassign current tile id (ie. move)
								$this->m_GameInfoDao->update($game);
								$this->look();
							} else {
								$this->log('Seems this door here is blocking my way.. Might be able to use a key if i have one.');
							}

						} else {
							$this->log('What\'s that?  Type help to get a list of available commands.');
						}
						break;
					}
					case 'look': {
						$this->look();
						break;
					}
					default:
						// switch($command) {
						// case 'debugmode': {
						// if(isset($param[0])) {
						// if($param[0] == 'on') {
						// $this->m_DebugModeEnabled = true;
						// } elseif($param[0] == 'off') {
						// $this->m_DebugModeEnabled = false;
						// } elseif($param[0] == 'toggle') {
						// $this->m_DebugModeEnabled = !($this->m_DebugModeEnabled);
						// }
						// }
						// $this->log('OK');
						// break;
						// }
						// default:
						// $this->log('What\'s that?  Type help to get a list of available commands.');
						// break;
						// }
						// if($this->m_DebugModeEnabled) {//debug/konami mode commands
						// switch($command) {
						// case 'debug-tile': {
						// $infostring = "Tile => uid:" . $this->m_Tile->tileUid . " n: " .
						// (isset($this->m_Tile->northtileUid) ? $this->m_Tile->northtileUid : "x ") . "
						// e: " . (isset($this->m_Tile->easttileUid) ? $this->m_Tile->easttileUid : "x ")
						// . " s: " . (isset($this->m_Tile->southtileUid) ? $this->m_Tile->southtileUid :
						// "x ") . " w: " . (isset($this->m_Tile->westtileUid) ?
						// $this->m_Tile->westtileUid : "x ");
						// $this->log($infostring, true);
						// break;
						// }
						// default: {
						// $this->log('What\'s that?  Type help to get a list of available commands.');
						// break;
						// }
						// }
						// }

						break;
				}
			} else {
				//create missing pieces that make up a valid set of game data
				if(!isset($game)) {//a game exists
					$newGame = new Game;
					$newGame->gameUid = 0;
					$newGame->tileUid = 1;
					$newGame->userUid = $this->m_User->userUid;
					$gamesResult = $games->insert($newGame);
					$this->m_Game = $newGame;
					$this->log('created new game', true);
				}

				$this->log('error, you\'re not on any tile! this should never happen!');
			}
		} else {// not logged in switch
			switch ($command) {
				case 'help':
					$availableCommands = "available commands: help, create, login";
					$this->log($availableCommands);

					break;
				case 'create':
					if(isset($params[0]) && isset($params[1]) && isset($params[2])) {
						if($params[1] == $params[2]) {
							if(!isset($this->m_UserDao)) {
								$this->m_UserDao = DAOFactory::getUserDAO();
							}
							//try and query for a user matching that name
							$this->m_User = $this->m_UserDao->queryByName($params[0]);

							if(!isset($this->m_User[0])) {//create new user if none with that name exist
								$this->m_User = new User;
								$this->m_User->userUid = NULL;
								$this->m_User->name = $params[0];
								$this->m_User->password = password_hash($params[1], PASSWORD_BCRYPT);
								$this->m_User->score = 0;
								@$this->m_UserDao->insert($this->m_User);

								$this->log('Good to have you, ' . $this->m_User->name . '! You can now log in with your name and password');
							} else {
								$this->m_User = null;
								$this->log('Apologies, \'' . $params[0] . '\' appears to be taken, please try a different one');
							}
						} else {
							$this->log('Apologies, your repeated password doesn\'t appear to match, please try again');
						}
					} else {
						$this->log('Here\'s how to do that: create &lt;UniqueUserName&gt; &lt;Password&gt; &ltRepeatPassword&gt;');
					}
					break;
				case 'login':
					if(isset($params[0]) && isset($params[1])) {
						if(!isset($this->m_UserDao)) {
							$this->m_UserDao = DAOFactory::getUserDAO();
						}
						$this->m_User = $this->m_UserDao->queryByName($params[0]);
						if(isset($this->m_User[0])) {
							$this->m_User = $this->m_User[0];
							if(password_verify($params[1], $this->m_User->password)) {
								$_SESSION['userid'] = $this->m_User->userUid;
								$this->log('Good to see you, ' . $this->m_User->name . '!');

								$games = DAOFactory::getGameDAO();
								$gamesResult = $games->queryByUserUid($this->m_User->userUid);
								if(isset($gamesResult[0])) {//a game exists
									$this->log('Game exists', true);
									$this->m_Game = $gamesResult[0];
								} else {//no games exist
									$newGame = new Game;
									$newGame->gameUid = 0;
									$newGame->tileUid = 1;
									$newGame->userUid = $this->m_User->userUid;
									$gamesResult = $games->insert($newGame);
									$this->m_Game = $newGame;
									$this->log('created new game', true);
								}
								$this->look();
							} else {
								$this->log('Wrong password.');
							}
						} else
							$this->log('unknown username and or password');
					} else {
						$this->log('Here\'s how to do that: login &lt;UserName&gt; &lt;Password&gt;');
					}
					break;
				case 'logout': {
					session_destroy();
					header("Location: index.php");
					break;
				}
				default:
					$this->log('What\'s that?  Type help to get a list of available commands.');
					break;
			}
		}

	}

	// Ad an item to the log array to be displayed on screen
	//@returns void
	//@params string $logItem the item to save in the array

	function log($logItem, $isDebugLog = false) {
		if($isDebugLog) {
			if($this->m_DebugModeEnabled) {
				array_push($this->m_log, '[debug] ' . $logItem);
			}
		} else {
			array_push($this->m_log, $logItem);

		}
	}

	// Save log itmes to the session.
	//@returns void
	//@params none
	function saveLogArray() {
		$_SESSION['log'] = implode("|", array_slice($this->m_log, -30));
	}

	//@returns void
	//@params int $direction
	function navigate($direction) {
		echo 'navigated';
	}

	//@returns string
	//@params none
	function encodeClientData() {

	}

	//3.114
	//@returns string
	//@params Tile.class.php Tile
	function look() {
		$game = $this->getGameFromUserId($this->m_User->userUid);

		$itemsOnTile = $this->getItemsForGameOnTile($game);
		$itemsInInventory = $this->getInventoryitemsForGame($game);
		$tileDao = DAOFactory::getTileDAO();
		$tileDescription = $tileDao->load($game->tileUid);
		$tileDescription = $tileDescription->description;
		$itemsOnTileNames = array();
		foreach($itemsOnTile as $ItemOnTile) {
			$matchFound = false;
			foreach($itemsInInventory as $invItem) {
				if($ItemOnTile->itemUid == $invItem->itemUid) {
					$matchFound = true;
					break;
				}
			}
			if(!$matchFound) {
				array_push($itemsOnTileNames, $ItemOnTile->name);
			}
			$matchFound = false;
		}
		if(isset($itemsOnTileNames[0])) {
			$description = $tileDescription . '. Around me I see: ' . implode(', ', $itemsOnTileNames) . '.';
		} else {
			$description = $tileDescription . '. There doesn\'t seem to be anything particularly interesting around here.';

		}
		$this->log($description);
	}

	//@returns void
	//@params none
	function pickupItem($item) {
		$game = $this->getGameFromUserId($this->m_User->userUid);

		$itemsOnTile = $this->getItemsForGameOnTile($game);
		$itemFound = FALSE;
		foreach($itemsOnTile as $itemOnTile) {
			if(strtolower($itemOnTile->name) == strtolower($item)) {
				if(!isset($this->m_InventoryItemDao)) {
					$this->m_InventoryItemDao = DAOFactory::getInventoryItemDAO();
				}
				$itemFound = TRUE;
				if($itemOnTile->pickupable) {
					if(!isset($this->m_ItemLocationDao)) {
						$this->m_ItemLocationDao = DAOFactory::getItemlocationDAO();
					}
					$this->m_ItemLocationDao->delete($game->gameUid, $itemOnTile->itemUid, $game->tileUid);
					$inventoryItem = new InventoryItem;
					$inventoryItem->gameUid = $game->gameUid;
					$inventoryItem->itemItemUid = $itemOnTile->itemUid;
					$this->m_InventoryItemDao->insert($inventoryItem);
					unset($inventoryItem);
					$this->log($item . ' put in inventory');
				} else {
					$this->log("I'm not going to take the $item, silly ;p");
				}
			}
		}
		if(!$itemFound) {
			$this->log("I don't see the $item anywhere, am I going crazy?");
		}
	}

	//@returns void
	//@params none
	function dropItem($item) {
		$game = $this->getGameFromUserId($this->m_User->userUid);

		if(!isset($this->m_InventoryItemDao)) {
			$this->m_InventoryItemDao = DAOFactory::getInventoryItemDAO();
		}
		if(!isset($this->m_ItemDao)) {
			$this->m_ItemDao = DAOFactory::getItemDAO();
		}
		$itemsFoundByName = $this->m_ItemDao->queryByName(strtolower($item));
		if(isset($itemsFoundByName[0])) {
			$invItem = $this->m_InventoryItemDao->load($game->gameUid, $itemsFoundByName[0]->itemUid);
			if(isset($invItem)) {
				if(!isset($this->m_ItemLocationDao)) {
					$this->m_ItemLocationDao = DAOFactory::getItemlocationDAO();
				}
				if($itemsFoundByName[0]->tileUid != $game->tileUid) {
					$itemlocation = new Itemlocation;
					$itemlocation->gameUid = $game->gameUid;
					$itemlocation->itemUid = $itemsFoundByName[0]->itemUid;
					$itemlocation->tileUid = $game->tileUid;
					$this->m_ItemLocationDao->insert($itemlocation);
				}
				$this->m_InventoryItemDao->delete($game->gameUid, $itemsFoundByName[0]->itemUid);
				$this->log($item . ' is dropped on the ground');
			} else {
				$this->log($item . ' not found in inventory');
			}
		} else {
			$this->log($item . ' not found in inventory');
		}
	}

	//@returns void
	//@params none
	function checkInventory() {
		$game = $this->getGameFromUserId($this->m_User->userUid);

		if(!isset($this->m_ItemDao)) {
			$this->m_ItemDao = DAOFactory::getItemDAO();
		}
		if(isset($inventoryItems[0])) {
			unset($inventoryItems);
		}
		if(isset($inventoryItem)) {
			unset($inventoryItem);
		}
		$inventoryItems = $this->getInventoryitemsForGame($game);
		if(isset($inventoryItems[0]) && !is_null($inventoryItems[0])) {
			$inventoryItemNames = array();
			foreach($inventoryItems as $inventoryItem) {
				//$item = $this->m_ItemDao->load($inventoryItem->itemItemUid);
				if($inventoryItem->isVisible) {
					array_push($inventoryItemNames, $inventoryItem->name);
				}
			}
			if(count($inventoryItemNames) > 0) {
				$inventoryItemNames = implode(', ', $inventoryItemNames);
				$this->log('I have: ' . $inventoryItemNames);
			} else {
				$this->log('My inventory is empty');
			}

		} else {
			$this->log('Nothing in there at the moment');
		}

	}

	//@returns Array() of items? maybe just return their Id's?
	//@params int $gameId
	//        int $tileId
	function getItemsForGameOnTile($game) {
		$itemLocationDaoExt = new ItemLocationDaoExt();
		$movedFromHereItems = $itemLocationDaoExt->queryGameId($game->gameUid);
		$movedItems = $itemLocationDaoExt->queryGameIdAndTileId($game->gameUid, $game->tileUid);

		if(!isset($this->m_ItemDao)) {
			$this->m_ItemDao = DAOFactory::getItemDAO();
		}

		$tileItems = $this->m_ItemDao->queryByTileUid($game->tileUid);
		$inventoryItems = $this->getInventoryItemsForGame($game);
		$itemsOnTile = Array();
		foreach($tileItems as $tileItem) {
			$foundMatch = false;
			if(isset($movedFromHereItems)) {
				foreach($movedFromHereItems as $movedItem) {
					if($tileItem->itemUid == $movedItem->itemUid) {
						$foundMatch = true;
					}
				}
			}
			if(isset($inventoryItems)) {
				foreach($inventoryItems as $inventoryItem) {
					if($tileItem->itemUid == $inventoryItem->itemUid) {
						$foundMatch = true;
					}
				}
			}
			if(!$foundMatch) {
				array_push($itemsOnTile, $tileItem);
			}
		}
		foreach($movedItems as $movedItem) {
			array_push($itemsOnTile, $this->m_ItemDao->load($movedItem->itemUid));
		}
		return $itemsOnTile;
	}

	//@returns void
	//@params string $itemName
	function examineItem($itemName) {
		$game = $this->getGameFromUserId($this->m_User->userUid);
		if(!isset($this->m_ItemDao)) {
			$this->m_ItemDao = DAOFactory::getItemDAO();
		}
		$itemLowerCase = strtolower($itemName);
		$inventoryItems = $this->getInventoryItemsForGame($game);
		$itemsOnTile = $this->getItemsForGameOnTile($game);

		if(isset($inventoryItems)) {
			foreach($inventoryItems as $inventoryItem) {
				if($inventoryItem->name == $itemLowerCase) {
					$this->log($inventoryItem->description);
					return;

				}

				//array_push($itemsOnTile, $inventoryItem);
			}
		}
		if(isset($itemsOnTile)) {
			foreach($itemsOnTile as $itemOnTile) {
				if($itemOnTile->name == $itemLowerCase) {
					$this->log($itemOnTile->description);
					return;
				}
			}
		}
		$this->log("No $itemName around here..");
	}

	function getLockedDoors($game) {
		$lockedDoors = null;
		$itemsOnTile = $this->getItemsForGameOnTile($game);
		$inventoryItems = $this->getInventoryItemsForGame($game);
		if(isset($inventoryItems)) {
			$lockedDoors = array();
			//collect keys and doors on this tile.. shouldn't be that many
			foreach($inventoryItems as $inventoryItem) {
				foreach($itemsOnTile as $anItemOnThisTile) {
					if($inventoryItem->itemUid != $anItemOnThisTile->itemUid) {
						switch($anItemOnThisTile->itemUid) {
							case 7:
							//my front door - inside
							case 8:
							//my front door - outside
							case 11:
							//neighbor front door - outside
							case 21:
								//neighbor front door - inside
								array_push($lockedDoors, $anItemOnThisTile);
								break;
							default:
								break;
						}
					}
				}
			}
		}
		return $lockedDoors;
	}

	function useItem($item) {
		switch($item->itemUid) {
		}
	}

	function getInventoryitemsForGame($game) {

		if(isset($game)) {
			if(!isset($this->m_InventoryItemDao)) {
				$this->m_InventoryItemDao = new InventoryItemDaoExt();
			}
			$queryResults = $this->m_InventoryItemDao->queryInventoryForGameId($game->gameUid);
			if(!isset($this->m_ItemDao)) {
				$this->m_ItemDao = DAOFactory::getItemDAO();
			}
			if(isset($queryResults)) {
				$items = array();
				foreach($queryResults as $item) {
					array_push($items, $this->m_ItemDao->load($item->itemItemUid));
				}
				return $items;
			}
		}
		return null;
	}

	function getGameDao() {
		if(!isset($this->m_GameInfoDao) || !is_null($this->m_GameInfoDao)) {
			$this->m_GameInfoDao = DAOFactory::getGameDAO();
		}
		return $this->m_GameInfoDao;
	}

	function getGameFromUserId($userId) {
		if(!isset($this->m_Game) || !is_null($this->m_Game)) {
			$this->m_Game = $this->getGameDao()->queryByUserUid($userId);
			if(isset($this->m_Game[0])) {
				$this->m_Game = $this->m_Game[0];
			}
		}
		return $this->m_Game;
	}

}
?>