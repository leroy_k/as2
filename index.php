<?php
session_start();
require_once ("GameController.php");
$controller = GameController::getInstance();
// Get log out of session
if(isset($_SESSION['log'])) {
	$logitems = explode("|", $_SESSION['log']);
	foreach($logitems as $logitem) {
		$controller->log($logitem);
	}
}
// Check if player is logged in in the session
if(isset($_SESSION['userid'])) {
	$controller->m_isLoggedIn = TRUE;
} else {
	$controller->m_isLoggedIn = FALSE;
}
// Process Command
if(isset($_POST['command'])) {
	$controller->processCommand($_POST['command']);
}
// Save log items to session
$controller->saveLogArray();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Zork - 80x24</title>
		<meta http-equiv="Content-Language" content="en-us" />
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<link rel="stylesheet" href="reset.css" />
		<link rel="stylesheet" href="styles.css" />
	</head>
	<body>
		<div id="infobar">
			<div id="titledisplay" class="infocomponent">
				West of House
			</div>
			<div id="scoredisplay" class="infocomponent">
				Score: 0
			</div>
			<div id="movesdisplay" class="infocomponent">
				Moves: 0
			</div>
			<div style="clear: both"></div>
		</div>
		<div id="messagelog">
			<ul>
				<li>
					ZORK 0: The great IGAD Empire
				</li>
				<li>
					Copyright (c) 2014, Robin Goemaat, Leroy Ketelaars. No rights reserved.
				</li>
				<li>
					Revision 0 / Serial number 840726
				</li>
				<li>
					&nbsp;
				</li>
				<?php
				// Output Log items to screen
				foreach($controller->m_log as $logItem) {
					echo "<li>$logItem</li>";
				}
				?>
			</ul>
		</div>
		<div id="commandbar">
			<div id="icon">
				&gt;
			</div>
			<form method="post">
				<input type="text" name="command" id="input"/>
			</form>
		</div>
		<script type="text/javascript">
			var commandbar = document.getElementById("input");
			commandbar.focus();
			commandbar.onblur = function() {
				document.getElementById("input").focus();
			};
		</script>
	</body>
</html>
