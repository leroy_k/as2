<?php
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>WP2 - Zork Assignment</title>
	<meta http-equiv="Content-Language" content="en-us" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="reset.css" />
	<link rel="stylesheet" href="styles.css" />
	</head>
	<body>
	    <div id="infobar">
	        <div id="titledisplay" class="infocomponent">West of House</div>
	        <div id="scoredisplay" class="infocomponent">Score: 0</div>
	        <div id="movesdisplay" class="infocomponent">Moves: 0</div>
	        <div style="clear: both"></div>
	    </div>
	    <div id="messagelog">
	        <ul>
	            <li>ZORK 0: The great IGAD Empire</li>
	            <li>Copyright (c) 2014, Robin Goemaat, Leroy Ketelaars. No rights reserved.</li>
	            <li>Revision 0 / Serial number 840726</li>
	            <li>&nbsp;</li>
	            <li>My livingroom</li>
	        </ul>
	    </div>
	    <div id="commandbar">
	        <div id="icon">&gt;</div>
	        <form method="get">
	            <input type="text" name="command" />
	        </form>
	    </div>
	</body>
	</html>';
?>