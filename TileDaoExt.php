<?php
require_once ('class/dao/TileDAO.class.php');
class TileDaoExt extends TileMySqlDAO {
	public function queryByTileUid($value) {
		$sql = 'SELECT * FROM Tile WHERE Tile_uid = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($value);
		return $this->getList($sqlQuery);
	}
};
?>