-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2014 at 05:27 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `101865_081049zork`
--
CREATE DATABASE IF NOT EXISTS `101865_081049zork` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE 101865_081049zork;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `Game_uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id for each game',
  `User_uid` int(11) NOT NULL COMMENT 'identifies owner of each game',
  `Tile_uid` int(11) NOT NULL COMMENT 'location of the player in this game. room is determined from room_uid',
  PRIMARY KEY (`Game_uid`),
  UNIQUE KEY `uid_UNIQUE` (`Game_uid`),
  KEY `fk_Game_Player_idx` (`User_uid`),
  KEY `fk_Game_Tile1_idx` (`Tile_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventoryitem`
--

CREATE TABLE IF NOT EXISTS `inventoryitem` (
  `Game_uid` int(11) NOT NULL COMMENT 'identifies the game this inventory is part of',
  `Item_item_uid` int(11) NOT NULL COMMENT 'uid''s of items in inventory',
  PRIMARY KEY (`Game_uid`,`Item_item_uid`),
  KEY `fk_InventoryObject_Item1_idx` (`Item_item_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `Item_uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique item id',
  `name` varchar(45) NOT NULL COMMENT 'name of item',
  `Tile_uid` int(11) NOT NULL COMMENT 'default tile uid',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'if item is visible when looking at inventory',
  `pickupable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Determins if the item can be pickedup',
  `description` varchar(160) NOT NULL COMMENT 'Item description',
  PRIMARY KEY (`Item_uid`),
  UNIQUE KEY `uid_UNIQUE` (`Item_uid`),
  KEY `fk_Item_Tile1_idx` (`Tile_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`Item_uid`, `name`, `Tile_uid`, `isVisible`, `pickupable`, `description`) VALUES
(1, 'yesterday''s breakfast', 1, 1, 1, 'Leftover breakfast from yesterday some cereal. I think the milk has gone sour I can smell it from here.'),
(2, 'mirror', 1, 1, 0, 'I look awful why did that noise have to start today, while yesterday was such a long night.'),
(3, 'bed', 1, 1, 0, 'I could maybe catch some more zees in there  but it will be kind of hard with all that noise.'),
(4, 'phone', 1, 1, 1, 'Ah maybe I can call my neighbor and ask what’s up.'),
(5, 'house keys', 1, 1, 1, 'My front door keys. I could use them to unlock my door.'),
(6, 'jacket', 1, 1, 1, 'I don’t think I need my jacket it has been pretty good weather the past few days.'),
(7, 'front door', 1, 0, 0, 'The door, I think I locked it last night.'),
(8, 'my front door', 2, 0, 0, 'It''s the front door of my house.'),
(9, 'my car', 2, 1, 0, 'I need to get a new tail light don’t feel like getting a ticket.'),
(10, 'mailbox', 2, 1, 0, 'No mail here. Not too surprising since it’s sunday.'),
(11, 'neighbor’s front door', 3, 0, 0, 'Neighbor’s front door where the noise comes from. No use in knocking since he is not home. It seems to be locked maybe he left a key somewhere.'),
(12, 'door mat', 3, 1, 0, 'There is a little bit of a bulge underneath the mat, after I look underneath I reveal a key.'),
(13, 'key', 3, 1, 1, 'Maybe this is the key to the frontdoor.'),
(14, 'neighbor’s mailbox', 3, 1, 0, 'Empty, he must have emptied it before he left.'),
(15, 'toilet', 4, 1, 0, 'A toilet. I don''t need to go right now.'),
(16, 'living room door', 4, 0, 0, 'The noise is definitely coming from behind this door.'),
(17, 'staircase', 4, 1, 1, 'A staircase to upstairs, but no need to go there.'),
(18, 'sofa', 5, 1, 0, 'A nice looking leather sofa.'),
(19, 'radio', 5, 1, 0, 'The radio that’s the noise maker it seems to be set on a clock he must use it as some sort of alarm clock.'),
(20, 'coffee table', 5, 1, 0, 'A glass coffee table.'),
(21, 'neighbor''s front door', 4, 0, 0, 'Front door to outside.'),
(22, 'living room door', 5, 0, 0, 'Door from the livingroom to the hallway.');

-- --------------------------------------------------------

--
-- Table structure for table `itemlocation`
--

CREATE TABLE IF NOT EXISTS `itemlocation` (
  `Game_uid` int(11) NOT NULL,
  `Item_uid` int(11) NOT NULL,
  `Tile_uid` int(11) NOT NULL,
  PRIMARY KEY (`Game_uid`,`Item_uid`,`Tile_uid`),
  KEY `fk_ItemLocation_Game1_idx` (`Game_uid`),
  KEY `fk_ItemLocation_Item1_idx` (`Item_uid`),
  KEY `fk_ItemLocation_Tile1_idx` (`Tile_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tile`
--

CREATE TABLE IF NOT EXISTS `tile` (
  `Tile_uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id for every tile',
  `northtile_uid` int(11) DEFAULT NULL COMMENT 'unique id of the tile north of this one, or null if none',
  `easttile_uid` int(11) DEFAULT NULL COMMENT 'unique id of the tile east of this one, or null if none',
  `southtile_uid` int(11) DEFAULT NULL COMMENT 'unique id of the tile south of this one, or null if none',
  `westtile_uid` int(11) DEFAULT NULL COMMENT 'unique id of the tile west of this one, or null if none',
  `description` varchar(300) NOT NULL,
  PRIMARY KEY (`Tile_uid`),
  UNIQUE KEY `uid_UNIQUE` (`Tile_uid`),
  KEY `fk_Tile_Tile1_idx` (`northtile_uid`),
  KEY `fk_Tile_Tile2_idx` (`easttile_uid`),
  KEY `fk_Tile_Tile3_idx` (`southtile_uid`),
  KEY `fk_Tile_Tile4_idx` (`westtile_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tile`
--

INSERT INTO `tile` (`Tile_uid`, `northtile_uid`, `easttile_uid`, `southtile_uid`, `westtile_uid`, `description`) VALUES
(1, 2, NULL, NULL, NULL, 'My house: What is that noise? I wanted to sleep in. I think the neighbor left his radio on when he left. Maybe I can do something about that. To the north is my front-door.'),
(2, NULL, 3, 1, NULL, 'Outside the house: Aaaaaaaaah some fresh air does the body good! That noise is louder out here. It''s definitely the radio coming from the east. To the east is my neighbor’s house and to the south is my house.'),
(3, NULL, NULL, 4, 2, 'Outside Neighbor’s house: My neighbor’s car is not here. He must have taken it to the airport. To the south is my neighbors house and to the west is my house.'),
(4, 3, NULL, 5, NULL, 'Neighbor’s hallway: Finally inside! He must have left in a hurry because that radio is crazy-loud. How do you forget to turn that off?! Hm.. seems he’s redecorated the place a bit. The living room is to the south, front door to the north'),
(5, 4, NULL, NULL, NULL, 'Neighbor’s living room: Well I am finally here I just have to turn of the radio and I can crawl back in my bed. To the nord is the hallway.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `User_uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id for each player',
  `name` varchar(45) NOT NULL COMMENT 'unique player login name',
  `password` varchar(60) NOT NULL COMMENT 'hashed player login password',
  `score` int(11) NOT NULL COMMENT 'score is the amount of times this player has completed the game',
  PRIMARY KEY (`User_uid`),
  UNIQUE KEY `uid_UNIQUE` (`User_uid`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `fk_Game_Player` FOREIGN KEY (`User_uid`) REFERENCES `user` (`User_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Game_Tile1` FOREIGN KEY (`Tile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventoryitem`
--
ALTER TABLE `inventoryitem`
  ADD CONSTRAINT `fk_InventoryObject_Game1` FOREIGN KEY (`Game_uid`) REFERENCES `game` (`Game_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_InventoryObject_Item1` FOREIGN KEY (`Item_item_uid`) REFERENCES `item` (`Item_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_Item_Tile1` FOREIGN KEY (`Tile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `itemlocation`
--
ALTER TABLE `itemlocation`
  ADD CONSTRAINT `fk_ItemLocation_Game1` FOREIGN KEY (`Game_uid`) REFERENCES `game` (`Game_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ItemLocation_Item1` FOREIGN KEY (`Item_uid`) REFERENCES `item` (`Item_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ItemLocation_Tile1` FOREIGN KEY (`Tile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tile`
--
ALTER TABLE `tile`
  ADD CONSTRAINT `fk_Tile_Tile1` FOREIGN KEY (`northtile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Tile_Tile2` FOREIGN KEY (`easttile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Tile_Tile3` FOREIGN KEY (`southtile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Tile_Tile4` FOREIGN KEY (`westtile_uid`) REFERENCES `tile` (`Tile_uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
