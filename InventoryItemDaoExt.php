<?php
//require_once ('class/dao/InventoryItemDAO.class.php');
class InventoryItemDaoExt extends InventoryItemMySqlDAO {

	public function queryInventoryForGameId($gameUid) {
		$sql = 'SELECT * FROM InventoryItem WHERE Game_uid = ?';
		// $sql = "SELECT Item_uid, name, Tile_uid, isVisible, pickupable, description FROM inventoryitem, item WHERE 'Game_uid'= ? AND Item_uid = Item_item_uid";
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($gameUid);
		return $this->getList($sqlQuery);
	}

}
?>