<?php

//require_once ('class/dao/ItemLocationDAO.class.php');
class ItemLocationDaoExt extends ItemLocationMySqlDAO {
	public function queryGameIdAndTileId($gameId, $tileId) {
		//SELECT *  FROM `InventoryItem` WHERE Item_item_uid = 1 AND Game_uid = 0
		$sql = "SELECT * FROM `ItemLocation` WHERE Tile_uid = ? AND Game_uid = ?";
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($tileId);
		$sqlQuery->setNumber($gameId);
		return $this->getList($sqlQuery);
	}

	public function queryGameId($gameId) {
		$sql = 'SELECT * FROM `ItemLocation` WHERE Game_uid = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($gameId);
		return $this->getList($sqlQuery);
	}
}
?>